#include <boost/range/any_range.hpp>

#include <list>
#include <vector>
#include <iostream>

typedef boost::any_range<int,
                         boost::bidirectional_traversal_tag,
                         int,
                         std::ptrdiff_t
                        > int_any_range;


int_any_range make_range(bool use_list) {
    static std::vector<int> vec = {1, 2, 3, 4};
    static std::list<int> list = {5, 6, 7};

    if (use_list) {
        return list;
    } else { return vec; }
}

int main() {
    for (int i : make_range(true)) {
        std::cout << i << " ";
    }

    for (int i : make_range(false)) {
        std::cout << i << " ";
    }

    std::cout << std::endl;
}

