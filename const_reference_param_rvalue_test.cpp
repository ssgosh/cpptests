#include <iostream>

using namespace std;

void foo(const int &param)
{
    cout << param << endl;
}

int main()
{
    int x = 10;
    foo(x);
    foo(5);
    return 0;
}

