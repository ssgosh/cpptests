#include <vector>
#include <iostream>

typedef std::vector<int> vec_t;

vec_t f() {
    return vec_t(1024);
}

int main()
{
    for (auto i : f()) {
        std::cout << i << " ";
    }

    std::cout << std::endl;
    return 0;
}
